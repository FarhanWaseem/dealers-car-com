package LeadSubmission;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LeadElements {

// Root and Free Style Elements

    @FindBy (how = How.ID, using = "makeSelect")
    protected WebElement Make;

    @FindBy(how = How.XPATH, using = "//select[@id='makeSelect']/option[2]")
    protected WebElement MakeSelect;

    @FindBy(how = How.ID, using = "modelSelect")
    protected WebElement Model;

    @FindBy(how = How.XPATH,using = "//select[@id='modelSelect']/ option[2]" )
    protected WebElement ModelSelect;

    @FindBy(how = How.ID, using = "trimSelect")
    protected WebElement Trim;

    @FindBy(how = How.XPATH, using = "//select[@id='trimSelect']/ option[2]")
    protected WebElement TrimSelect;

    @FindBy(how = How.ID, using = "zipcodetext")
    protected WebElement Zipcode;

    @FindBy(how = How.ID, using = "btn-getquote")
    protected WebElement GetStartedButton;


    //// Quote Page Elements

    @FindBy(how = How.ID, using = "chkAll")
    protected WebElement Dealers;

    @FindBy(how = How.ID, using = "FirstNameText")
    protected WebElement Firstname;

    @FindBy(how = How.ID, using = "LastNameText")
    protected WebElement Lastname;

    @FindBy(how = How.ID,using = "AddressText")
    protected WebElement Address;

    @FindBy(how = How.ID, using = "PhoneAreaText")
    protected WebElement Phone1;

    @FindBy(how = How.ID, using = "PhonePrefixText")
    protected WebElement Phone2;

    @FindBy(how = How.ID, using = "PhoneChText")
    protected WebElement Phone3;

    @FindBy(how = How.ID, using = "EmailText")
    protected WebElement Email;

    @FindBy(how = How.ID, using = "btn-getquote")
    protected WebElement GetStartedQuote;


    // Thankyou Page Elements

    @FindBy(how = How.ID, using = "btnAgree")
    protected WebElement Iconsent;

    @FindBy(how = How.ID,using = "Make")
    protected WebElement MakeTYP;

    @FindBy(how = How.XPATH, using = "//select[@id='Make']/option[15]")
    protected WebElement MakeSelectTYP;

    @FindBy(how = How.ID, using = "Model")
    protected WebElement ModelTYP;

    @FindBy(how = How.XPATH, using = "//select[@id='Model']/option[2]")
    protected WebElement ModelSelectTYP;

    @FindBy(how = How.ID, using = "submit")
    protected WebElement GoTYP;



}
