package BaseClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;


/**
 * Created by FarhanW @ 28-01-2019.
 */

public class DriverBaseClass {

    protected WebDriver driver;
    protected WebDriverWait wait;



    public static WebDriver chrome_driver() {
        System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
        return null;
    }

    public static WebDriver firefox_driver(){
        System.setProperty("webdriver.gecko.driver", "Drivers/geckodriver.exe");
        return null;
    }

    public static WebDriver IE_driver(){
        System.setProperty("webdriver.ie.driver", "Drivers/IEDriverServer.exe");
        return null;
    }

    @Parameters("Browser")
    @BeforeClass(alwaysRun = true)
    public void initDriver(String Browser) {

        if(Browser.equalsIgnoreCase("Chrome")) {

            ChromeOptions options = new ChromeOptions();
            options.addArguments("headless");
            options.addArguments("window-size=1200x600");
            driver = DriverBaseClass.chrome_driver();
            driver = new ChromeDriver(options);
            wait = new WebDriverWait(driver, 15, 1000);
            driver.manage().window().maximize();

        }

        else if (Browser.equalsIgnoreCase("Firefox")) {

            FirefoxOptions options = new FirefoxOptions();
            options.addArguments("--headless");
            driver = DriverBaseClass.firefox_driver();
            driver = new FirefoxDriver(options);
            wait = new WebDriverWait(driver, 15, 1000);
            driver.manage().window().maximize();

        }

        else if (Browser.equalsIgnoreCase("Internet Explorer")) {

            driver = DriverBaseClass.IE_driver();
            driver = new InternetExplorerDriver();
            wait = new WebDriverWait(driver, 15, 1000);
            driver.manage().window().maximize();

        }

    }




    @AfterClass(alwaysRun = true)
   public void closeDriver() {
        driver.close();
    }
}
