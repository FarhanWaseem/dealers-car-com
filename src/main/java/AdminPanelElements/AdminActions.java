package AdminPanelElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import Utilities.CommonActions;
import Utilities.GlobalVariables;

public class AdminActions extends AdminElements {

    private WebDriver driver;
    private WebDriverWait wait;
    private CommonActions commonActions;
    //private String RandomPath = CreateRandomPath.generateTestName();


    public AdminActions(WebDriver driver, WebDriverWait wait) {
        super();
        this.driver = driver;
        this.wait = wait;
        commonActions = new CommonActions(driver, wait);
        PageFactory.initElements(driver, this);
    }


    public void AdminLogin(String username, String password) throws InterruptedException {

        commonActions.navigateToUrl(GlobalVariables.adminhost);
        fillusername(username);
        fillpassword(password);
        checkadminlogin();
        clickloginadmin();

    }

    public void CreatePath( String pathname, String pathlabel) throws InterruptedException {

        Thread.sleep(4000);
        clickcreatepath();
        fillpathname(pathname);Thread.sleep(2000);
        clicktemplatedropdown();
        fillpathlabel(pathlabel);
        clicksavepath();
        Thread.sleep(5000);

    }
    public void DeletePath()throws InterruptedException{

        Thread.sleep(3000);
        clickpathcreatedbyit();
        Thread.sleep(4000);
        clicksortpathdiv();
        Thread.sleep(3000);
        clicksortpathdiv();Thread.sleep(3000);
        clickdeleteicon();Thread.sleep(3000);
        clickdeletcofirmation();Thread.sleep(4000);
    }

    private void fillusername(String textusername) throws InterruptedException {
        commonActions.fillInput(Username, textusername);}

    private void fillpassword(String textpassword) throws InterruptedException {
        commonActions.fillInput(Password, textpassword);}

    private void clickloginadmin() {
        commonActions.clickElement(LoginButton);
    }
    public void checkadminlogin() throws InterruptedException { commonActions.checkVisibilityOfElement(LoginButton);}



    private void clickcreatepath() {
        commonActions.clickElement(CreatePathButton);}

    private void clicktemplatedropdown(){
        Select sel = new Select(Templatedropdown);
        sel.selectByIndex(1);}
    private void clicktemplate(){commonActions.clickElement(FreestyleTemplate);}

    private void clicksavepath() {
        commonActions.clickElement(SavePathButton);
    }

    private void fillpathname(String textpathname) throws InterruptedException {
        commonActions.fillInput(PathNamefield, textpathname);}

    private void fillpathlabel(String textpathlabel) throws InterruptedException{
        commonActions.fillInput(PathLabelField, textpathlabel);}

    private void clickpathcreatedbyit(){commonActions.clickElement(PathCreatedByIT);}
    private void clicksortpathdiv(){commonActions.clickElement(sortpath);}

    private void clickpathlocation(){commonActions.clickElement(PathsLocation);}
    private void clickdeleteicon(){commonActions.clickElement(DeleteIcon);}
    private void clickdeletcofirmation(){commonActions.clickElement(DeleteConfirm);}

    }





